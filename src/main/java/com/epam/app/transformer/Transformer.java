package com.epam.app.transformer;

import com.epam.app.model.annotation.Column;
import com.epam.app.model.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(ResultSet rs)
            throws SQLException {
        Object obj = null;
        try {
            obj = clazz.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (clazz.isAnnotationPresent(Table.class)) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                if (f.isAnnotationPresent(Column.class)) {
                    Column column = f.getAnnotation(Column.class);
                    String name = column.name();
                    f.setAccessible(true);
                    try {
                        fillInField(rs, obj, name, f);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return obj;
    }

    private void fillInField(ResultSet rs, Object obj, String name, Field field) throws IllegalAccessException, SQLException {
        Class fieldType = field.getType();
        if (fieldType == String.class) {
            field.set(obj, rs.getString(name));
        } else if (fieldType == int.class) {
            field.set(obj, rs.getInt(name));
        } else if (fieldType == double.class) {
            field.set(obj, rs.getDouble(name));
        }
    }
}