package com.epam.app.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection {
    private static final String LOGIN = "root";
    private static final String PASSWORD = "root";
    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/ticketdb?useSSL=false";
    private static Connection connection = null;

    private DataBaseConnection() {

    }
    public static Connection getDBConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(CONNECTION_URL, LOGIN, PASSWORD);
                System.out.println("DB is connected");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
