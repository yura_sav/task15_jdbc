package com.epam.app.service;

import com.epam.app.DAO.implementation.CustomerDAOImpl;
import com.epam.app.model.ticket.Customer;

import java.util.Set;

public class ArtistService {
    public Customer findById(int id) {
        return new CustomerDAOImpl().getCustomer(id);
    }

    public Set<Customer> findAllCustomer() {
        return new CustomerDAOImpl().getAllCustomers();
    }

    public boolean insertCustomer(Customer customer) {
        return new CustomerDAOImpl().insertCustomer(customer);
    }

    public boolean deleteCustomer(int id) {
        if (id == 1) {
            return false;
        }
        return new CustomerDAOImpl().deleteCustomer(id);
    }

    public boolean updateCustomer(Customer customer) {
        return new CustomerDAOImpl().updateCustomer(customer);
    }
}
