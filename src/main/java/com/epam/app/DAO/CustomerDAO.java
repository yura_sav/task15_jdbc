package com.epam.app.DAO;

import com.epam.app.model.ticket.Customer;

import java.util.Set;

public interface CustomerDAO {
    Customer getCustomer(int id);

    Set<Customer> getAllCustomers();

    boolean insertCustomer(Customer customer);

    boolean updateCustomer(Customer customer);

    boolean deleteCustomer(int id);
}
