package com.epam.app.DAO;

import com.epam.app.model.ticket.Artist;

import java.util.Set;

public interface ArtistDAO {
    Artist getArtist(int id);

    Set<Artist> getAllArtists();

    boolean insertArtist(Artist artist);

    boolean updateArtist(Artist artist);

    boolean deleteArtist(int id);
}
