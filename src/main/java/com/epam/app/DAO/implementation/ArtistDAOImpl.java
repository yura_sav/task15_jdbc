package com.epam.app.DAO.implementation;

import com.epam.app.DAO.ArtistDAO;
import com.epam.app.connection.DataBaseConnection;
import com.epam.app.model.ticket.Artist;
import com.epam.app.model.ticket.Country;

import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Set;

public class ArtistDAOImpl implements ArtistDAO {
    @Override
    public Artist getArtist(int id) {
        Connection connection = DataBaseConnection.getDBConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM artist join country on artist.country_id = country.country_id WHERE artist_id=" + id);
            if (rs.next()) {
                return setArtist(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Set<Artist> getAllArtists() {
        Connection connection = DataBaseConnection.getDBConnection();
        Set<Artist> set = new LinkedHashSet<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM artist join country on artist.country_id = country.country_id");
            while (rs.next()) {
                set.add(setArtist(rs));
            }
            return set;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insertArtist(Artist artist) {
        Connection connection = DataBaseConnection.getDBConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT into artist values (?, ?, ?, ?)");
            preparedStatement.setInt(1, artist.getId());
            preparedStatement.setString(2, artist.getFirstName());
            preparedStatement.setString(3, artist.getLastName());
            preparedStatement.setInt(4, artist.getCountry().getId());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateArtist(Artist artist) {
        try {
            Connection connection = DataBaseConnection.getDBConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE artist SET artist_id=?, first_name=?, last_name=? WHERE country_id=?");
            ps.setInt(1, artist.getId());
            ps.setString(2, artist.getFirstName());
            ps.setString(3, artist.getLastName());
            ps.setInt(4, artist.getCountry().getId());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }


    @Override
    public boolean deleteArtist(int id) {
        {
            Connection connection = DataBaseConnection.getDBConnection();
            PreparedStatement preparedStatement;
            String sql = "DELETE FROM artist WHERE artist_id=" + id;
            try {
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.executeUpdate();
                int i = preparedStatement.executeUpdate();
                if (i == 1) {
                    return true;
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return false;
        }
    }

    private Artist setArtist(ResultSet rs) throws SQLException {
        Artist artist = new Artist();
        Country country = new Country();
        country.setId(rs.getInt("country_id"));
        country.setName(rs.getString("country_name"));
        artist.setId(rs.getInt("artist_id"));
        artist.setFirstName(rs.getString("first_name"));
        artist.setLastName(rs.getString("last_name"));
        artist.setCountry(country);
        return artist;
    }
}
