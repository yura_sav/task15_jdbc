package com.epam.app.DAO.implementation;

import com.epam.app.DAO.CustomerDAO;
import com.epam.app.connection.DataBaseConnection;
import com.epam.app.model.ticket.Customer;
import com.epam.app.transformer.Transformer;

import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Set;

public class CustomerDAOImpl implements CustomerDAO {
    @Override
    public Customer getCustomer(int id) {
        Connection connection = DataBaseConnection.getDBConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM customer WHERE customer_id=" + id);
            if (rs.next()) {
                Customer customer = new Customer();
               /* customer.setId(rs.getInt("customer_id"));
                customer.setFirstName(rs.getString("first_name"));
                customer.setLastName(rs.getString("last_name"));
                customer.setAge(rs.getInt("age"));*/
                customer = (Customer) new Transformer(Customer.class).fromResultSetToEntity(rs);
                return customer;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Set<Customer> getAllCustomers() {
        Connection connection = DataBaseConnection.getDBConnection();
        Set<Customer> set = new LinkedHashSet<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM customer");
            while (rs.next()) {

                set.add((Customer) new Transformer(Customer.class).fromResultSetToEntity(rs));

            }
            return set;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean insertCustomer(Customer customer) {
        Connection connection = DataBaseConnection.getDBConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT into customer values (? , ?, ?, ?)");
            preparedStatement.setInt(1, customer.getId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setInt(4, customer.getAge());
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean updateCustomer(Customer customer) {
        try {
            Connection connection = DataBaseConnection.getDBConnection();
            PreparedStatement ps = connection.prepareStatement("UPDATE customer SET first_name=?, last_name=?, age=? WHERE customer_id=?");
            ps.setString(1, customer.getFirstName());
            ps.setString(2, customer.getLastName());
            ps.setInt(3, customer.getAge());
            ps.setInt(4, customer.getId());
            int i = ps.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteCustomer(int id) {
        Connection connection = DataBaseConnection.getDBConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM customer WHERE customer_id=" + id);
            int i = preparedStatement.executeUpdate();
            if (i == 1) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}