package com.epam.app.model.ticket;

import java.math.BigDecimal;

public class Price {
    private int id;
    private BigDecimal cost;
    private int eventId;
    private int ticketTypeId;

    public Price(int id, BigDecimal cost, int eventId, int ticketTypeId) {
        this.id = id;
        this.cost = cost;
        this.eventId = eventId;
        this.ticketTypeId = ticketTypeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(int ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }
}
