package com.epam.app.model.ticket.type;

public class TicketType extends Type {

    public TicketType(int id, String type) {
        super(id, type);
    }
}
