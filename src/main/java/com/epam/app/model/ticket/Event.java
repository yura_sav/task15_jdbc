package com.epam.app.model.ticket;

import java.sql.Date;

public class Event {
    private int id;
    private Date date;
    private int freePlaces;
    private int eventTypeId;
    private  int artistId;

    public Event(int id, Date date, int freePlaces, int eventTypeId, int artistId) {
        this.id = id;
        this.date = date;
        this.freePlaces = freePlaces;
        this.eventTypeId = eventTypeId;
        this.artistId = artistId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getFreePlaces() {
        return freePlaces;
    }

    public void setFreePlaces(int freePlaces) {
        this.freePlaces = freePlaces;
    }

    public int getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }
}
