package com.epam.app.model.ticket;

import com.epam.app.model.annotation.Column;
import com.epam.app.model.annotation.Table;

@Table(name = "ticket")
public class Ticket {
    @Column(name = "ticket_id")
    private int id;
    @Column(name = "place_number")
    private int placeNumber;
    @Column(name = "status_type_id")
    private int statusTypeId;
    @Column(name = "oplata_type_id")
    private int oplataTypeId;
    @Column(name = "delivery_type_id")
    private int deliveryTypeId;
    @Column(name = "customer_id")
    private int customerId;
    @Column(name = "price_id")
    private int priceId;
    @Column(name = "event_id")
    private int eventId;
    @Column(name = "ticket_type_id")
    private int ticketTypeId;

    public Ticket(int id, int placeNumber, int statusTypeId, int oplataTypeId, int deliveryTypeId, int customerId, int priceId, int eventId, int ticketTypeId) {
        this.id = id;
        this.placeNumber = placeNumber;
        this.statusTypeId = statusTypeId;
        this.oplataTypeId = oplataTypeId;
        this.deliveryTypeId = deliveryTypeId;
        this.customerId = customerId;
        this.priceId = priceId;
        this.eventId = eventId;
        this.ticketTypeId = ticketTypeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlaceNumber() {
        return placeNumber;
    }

    public void setPlaceNumber(int placeNumber) {
        this.placeNumber = placeNumber;
    }

    public int getStatusTypeId() {
        return statusTypeId;
    }

    public void setStatusTypeId(int statusTypeId) {
        this.statusTypeId = statusTypeId;
    }

    public int getOplataTypeId() {
        return oplataTypeId;
    }

    public void setOplataTypeId(int oplataTypeId) {
        this.oplataTypeId = oplataTypeId;
    }

    public int getDeliveryTypeId() {
        return deliveryTypeId;
    }

    public void setDeliveryTypeId(int deliveryTypeId) {
        this.deliveryTypeId = deliveryTypeId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(int ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }
}
